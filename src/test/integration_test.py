import subprocess

def test_integration():
    # Making a copy of the app and the dbg
    subprocess.call(['mkdir', 'multiprocessing_stress_tester/'])
    subprocess.call(['cp',
                     '../main/multiprocessing_stress_tester/debugger.py',
                     'multiprocessing_stress_tester/debugger.py'])
    subprocess.call(['cp',
                     '../main/multiprocessing_stress_tester/app.py',
                     'multiprocessing_stress_tester/app.py'])
    from multiprocessing_stress_tester.app import submit
    with open('samplecpp/correct_generator.cpp', 'r') as gen_file:
        gen = gen_file.read()
    with open('samplecpp/correct_checker.cpp', 'r') as checker_file:
        chk = checker_file.read()
    with open('samplecpp/sol.cpp', 'r') as sol_file:
        correct = sol_file.read()

    # Sending a new test
    ans = submit([gen, chk, correct, correct], False, 100, 1)

    from multiprocessing_stress_tester.app import getSub
    checkSub = getSub(ans[0])

    # Rigth id
    assert(checkSub[0] == ans[0])
    # Right generator
    assert(checkSub[1] == gen)
    # Right checker
    assert(checkSub[2] == chk)
    # Right correct
    assert(checkSub[3] == correct)
    # Rigth wrong
    assert(checkSub[4] == correct)
    # Rigth result
    assert(checkSub[5] == ans[1])

    # Removing useless files
    subprocess.call(['rm', '-r', 'multiprocessing_stress_tester/'])
    subprocess.call(['rm', '-r', 'results/'])
