import subprocess 
import pytest

def test_unit():
    # Making a copy of the file to test
    subprocess.call(['cp', '../main/multiprocessing_stress_tester/debugger.py', '.'])
    from debugger import stresstest
    with open('comb.txt') as combinations:
        tests = combinations.readlines()
        for test in range(len(tests)):
            cpps = tests[test][0 : -1].split(' ')
            
            # Copying current cpps
            subprocess.call(['cp', 'samplecpp/' + cpps[0], 'generator.cpp'])
            subprocess.call(['cp', 'samplecpp/' + cpps[1], 'checker.cpp'])
            subprocess.call(['cp', 'samplecpp/' + cpps[2], 'correct.cpp'])
            subprocess.call(['cp', 'samplecpp/' + cpps[3], 'wrong.cpp'])

            result = stresstest(False, 100, 1)
            assert result == test
            
            # Removing current cpps
            subprocess.call(['rm', 'generator.cpp'])
            subprocess.call(['rm', 'checker.cpp'])
            subprocess.call(['rm', 'correct.cpp'])
            subprocess.call(['rm', 'wrong.cpp'])
    
    # Removing the copy of the tester
    subprocess.call(['rm', 'debugger.py', 'log.txt'])


