#include<bits/stdc++.h>

using namespace std;

mt19937_64 my_rand;
int rand(int l, int r) {
	return l + my_rand() % (r - l + 1);
}

int main(int argc, char **argv) {
	int seed = atoi(argv[1]);
	my_rand.seed(seed);
	int a = rand(1, 10), b = rand(1, 10);
	cout << a << ' ' << b << endl;
	return 0;
}
